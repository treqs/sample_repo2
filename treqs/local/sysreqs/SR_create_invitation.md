# Meeting Scheduler System

System requirements for a fictitious meeting scheduler system

## Detailed system requirements

The following requirements are derived from high level system requirements

### [requirement id=REQ0010 parent=REQ0001]

The system must support delivery of invitations via e-mail.

### [requirement id=REQ0011 parent=REQ0001]

The system must account for time-zone adjustments when
inviting others.
