# Manual test cases

## [testcase id=TC0001 story=US0006 requirement=REQ0006]

Purpose: Test that a resource can be scheduled

### Setup

### Scenario / Steps

1. Click the "book resource" button
2. Select resource type
3. Select start and end time of resource usage
4. Click "save" button

### Expected outcome

1. The resource is booked by the currently active user
2. The resource will not be available to other users before the end time of usage

## [testcase id=TC0003 story=US0001,US0002,US0007,US0005,US0004 requirement=REQ0007,REQ0009,REQ0010]

Purpose: Editing a system requirement and putting the change up for review

### Setup

### Scenario / Steps

1. Make a change to a file containing requirements with a text editor
2. Run "treqs" 
3. Run "git add FILE"
4. Run "git commit -m 'test'"
5. Run "git push origin HEAD:refs/for/master" to push the commit to Gerrit
6. Go to https://review.gerrithub.io/dashboard/self
7. Click the new change
8. Add a reviewer (note: this can be either a developer (US0001), a system manager (US0002,US0004) or a test architect(US0004))
9. Have that reviewer +2 the change
10. Submit the change

### Expected outcome

1. No errors are listed in the file logs/Summary...
2. The requirement file has been updated on https://github.com/regot-chalmers/treqs

### Tear down

1. Run "git log" and find the commit hash of the latest commit
2. Run "git revert HASH"
3. Push the new commit to Gerrit and submit it like above 

### Test result

Protocol of the result of executing this test, latest on top.

## [testcase id=TC0004 story=US0003 requirement=REQ0008]

Purpose: Creating a new requirement on a branch

### Setup

### Scenario / Steps

1. Go to the root of the treqs repo
2. Run "git checkout -b experimentalbranch" to create a new branch and switch to it 
3. Create a new requirements file requirements/SR-temp.md
4. Add "[requirement id=REQtemp story=UStemp]" to the file and save
5. Run "git add requirements/SR-temp.md"
6. Run "git commit -m 'my message'"

### Expected outcome

1. After running "treqs" on the new branch, the new requirement is listed in logs/SysReq...
2. A running "treqs" on the master branch, the new requirement is not listed in logs/SysReq...

### Tear down

1. Run "git checkout master"
2. Run "git branch -D experimentalbranch"

### Test result

Protocol of the result of executing this test, latest on top.
